#include <cassert>
#include <cstdio>
#include <libusb-1.0/libusb.h>

int main() {
    libusb_context *context = NULL;
    libusb_device **list = NULL;
    int rc = 0;
    ssize_t count = 0;

    rc = libusb_init(&context);
    assert(rc == 0);

    count = libusb_get_device_list(context, &list);
    assert(count > 0);

    for (ssize_t idx = 0; idx < count; ++idx) {
        libusb_device *device = list[idx];
        libusb_device_descriptor desc = {0};

        rc = libusb_get_device_descriptor(device, &desc);
        assert(rc == 0);

        printf("\nVendor:Device = %04x:%04x ", desc.idVendor, desc.idProduct);
        printf("%04x:%04x (bus %d, device %d)",
			desc.idVendor, desc.idProduct,
			libusb_get_bus_number(device), libusb_get_device_address(device));
        libusb_device_handle *handle;
        int status = libusb_open(device, &handle);
        if(status == 0) { 
            unsigned char manufacturer[200];
            libusb_get_string_descriptor_ascii(handle, desc.iManufacturer,
                manufacturer,200);
            printf("%s \n", manufacturer);
            libusb_close(handle);
        } else if(status == LIBUSB_ERROR_ACCESS) {
            printf("Access denied ");
        } else if(status == LIBUSB_ERROR_NO_MEM) {
            printf("Error no MEM\n");
        } else if(status == LIBUSB_ERROR_NO_DEVICE) {
            printf("Error no DEVICE");
        } else {
            printf("Error no MEM");
        }

        struct libusb_config_descriptor *config = NULL; 
        status = libusb_get_config_descriptor(device, 0, &config);
        if(status == 0 && config != NULL) { 

            for (int i = 0; i < config->bNumInterfaces; i++) {
                for (int j = 0; j < config->interface[i].num_altsetting; j++) {
                    int interfaceclass = (int)(config->interface[i].altsetting[j].bInterfaceClass);
                
                    printf("class descriptor %d %s\n", interfaceclass, (interfaceclass == LIBUSB_CLASS_HID) ? "HID" : "not hid");
                }
            }
            libusb_free_config_descriptor(config);
            
        } else {
            printf("descriptor denied ");
        }

        uint8_t path[8]; 
        int r = libusb_get_port_numbers(device, path, sizeof(path));
		if (r > 0) {
			printf(" path: %d", path[0]);
			for (int j = 1; j < r; j++)
				printf(".%d", path[j]);
		}

    }

    libusb_free_device_list(list, count);
    libusb_exit(context);
}

