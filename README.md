# USB detect

detect usb devices and pick out the HID devices

## use case

When an USB device is disconnected during or before the users application is running,
that is not detected.

The project should give a template to solve this situation by:
- detect which devives are connected
- detect if an specific USB devices is connected (or not)

## environment

The code should run on Linux and Windows.
